import socket, threading, signal, sys, redis
from cb import CircuitBreaker

configuration =  {
            "HOST_NAME" : "127.0.0.1",
            "BIND_PORT" : 8080,
            "MAX_REQUEST_LEN" : 1024,
            "CONNECTION_TIMEOUT" : 2
          }


class ProxyServer:
    def __init__(self, config):
        signal.signal(signal.SIGINT, self.closeSocket)   # Call SIGINT on keyboard Interrupt --->Close socket
        self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)      # Create socket
        self.serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serverSocket.bind((config['HOST_NAME'], config['BIND_PORT'])) # socket binding
        self.serverSocket.listen(10)    # start listening
        self.__clients = {}             #inistialize client dict as empty


    def waitForConnection(self):
        """ Wait for clients to connect """
        r = redis.Redis('127.0.0.1')
        activeServer = r.lrange('activeServer', 0, -1)
        activeServer = set(activeServer)
        activeServer = list(activeServer)  ##This removes the duplicate entries in the serverlist
        print activeServer
        totalServer = len(activeServer)

        request_count= totalServer -1 # Initializing with serverlist length -1 so that first request always go to first server in the list

        while True:
            (clientSocket, client_add) = self.serverSocket.accept()   # Establish the connection
            request_count=request_count+1

            server_address=activeServer[request_count%totalServer].split(':')
            urlname=activeServer[request_count%totalServer]
            url_server=server_address[0]
            #print url_server
            port=server_address[1]
            #print port
            try:
                createThread = threading.Thread(name=client_add, target=self.proxy_thread, args=(clientSocket, client_add, request_count,url_server ,port,urlname))
            except Exception as e:
                print e
            createThread.setDaemon(True)
            createThread.start()
        self.closeSocket(0, 0)


    def proxy_thread(self, conn, client_addr, i, ip, port, urlname):
        try:
            self.CB_thread(conn, client_addr, i, ip, port, urlname)
        except Exception as e:
            print 'Got Exception: ',e


    @CircuitBreaker(max_failure_to_open=3, reset_timeout=50)
    def CB_thread(self, conn, client_addr, i, ip, port, urlname):
        request = conn.recv(configuration['MAX_REQUEST_LEN'])        # get the request from client
        first_line = request.split('\n')[0]                   # parse the first line in request body


        try:
            print port
            mysocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            mysocket.settimeout(5)

            mysocket.connect((ip, int(port)))
            mysocket.sendall(request)                             # send request to host

            while 1:
                data = mysocket.recv(configuration['MAX_REQUEST_LEN'])   # receive data from host
                if (len(data) > 0):
                    conn.send(data)                               # send data to client
                else:
                    break
            mysocket.close()
            conn.close()
        except Exception as exc:
            print 'Got Exception: ',exc
            if mysocket:
                mysocket.close()
            if conn:
                conn.close()
            raise exc

    def closeSocket(self, signum, frame):
        """ Close Socket """
        self.serverSocket.close()
        sys.exit(0)


if __name__ == "__main__":
    server = ProxyServer(configuration)
    server.waitForConnection()